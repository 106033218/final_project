// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { monsterBoard } from "./monsterBoard";
const {ccclass, property} = cc._decorator;

@ccclass
export class firebowl extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    @property(cc.Prefab)
    bat: cc.Prefab = null;
    @property(cc.Prefab)
    bee: cc.Prefab = null;
    @property(cc.Prefab)
    slim: cc.Prefab = null;
    @property(cc.Prefab)
    spider: cc.Prefab = null;

    @property(cc.Node)
    other1:cc.Node = null;
    @property(cc.Node)
    other2:cc.Node = null;

    UI:cc.Node = null;

    @property(monsterBoard)
    monsterBoard: any ;

    private crossNode:cc.Node = null;
    private userPrompt:cc.Node = null;
    private player:cc.Node = null;
    private monsterBoardNode:cc.Node = null;
    private playerIn:boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initChildNode();
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

    }

    start () {
        this.crossNode.on(cc.Node.EventType.MOUSE_UP, ()=>{
            this.closeBoard(this.monsterBoard.returnMonsterId());
        }, this);
    }

    // update (dt) {}


    initChildNode(){
        this.UI = cc.find("Canvas/UI");
        this.crossNode = cc.find("monsterBoard/cross/checkbox_01", this.node);
        this.userPrompt = this.node.getChildByName("userPrompt");
        this.userPrompt.opacity = 0;
        this.monsterBoardNode = this.node.getChildByName("monsterBoard");
        this.monsterBoardNode.active = false;
    }



    onCollisionEnter(other, self) {
        cc.log("onCollisionEnter");
        if(other.node.name == "player"){
            this.userPrompt.opacity = 240;
            this.player = other.node;
            this.playerIn = true;
        }
    }


    onCollisionExit(other, self) {
        cc.log("onCollisionExit");
        if(other.node.name == "player"){
            this.userPrompt.opacity = 0;
            this.playerIn = false;
        }
    }



    generate_monster(monsterId){
        var string = "generate_monster  " + monsterId;
        cc.log(string);
        var monster;
        var parent = this.node.parent;
        switch(monsterId){
            case 1:
                monster = cc.instantiate(this.bat);
                break;
            case 2:
                monster = cc.instantiate(this.bee);
                break;
            case 3:
                monster = cc.instantiate(this.slim);
                break;
            case 4:
                monster = cc.instantiate(this.spider);
                break;
            default:
                cc.log("something wrong");
        }
        monster.position = this.node.position;
        parent.addChild(monster);
    }

    test(){
        cc.log("test_success");
    }



    onKeyDown(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.u:    
                break;

            
                
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.i:
                cc.log("u");
                if(this.monsterBoardNode.active == true){
                    this.closeBoard(this.monsterBoard.returnMonsterId());
                }
                else{
                    if(this.playerIn){
                        this.openBoard();
                    }
                }

            
                break;

            case cc.macro.KEY.q:
                // var state = "";
                // state +=  "playerIn:";
                // state += this.playerIn;
                // cc.log(state);
                // state = "";
                // state += "monsterBoardNode active:";
                // state += this.monsterBoardNode.active;
                // cc.log(state);
                cc.log(this.player);
                break;




            

            
        }
    }

    openBoard(){
        cc.log("open");
        this.monsterBoardNode.active =  true;
        this.UI.active = false;
        this.player.active = false;
        this.other2.opacity = this.other1.opacity = 0;
    }

    closeBoard(monsterId){
        cc.log("close");
        this.generate_monster(monsterId);
        this.monsterBoardNode.active =  false;
        this.UI.active = true;
        this.player.active = true;
        this.other2.opacity = this.other1.opacity = 255;
    }
}



