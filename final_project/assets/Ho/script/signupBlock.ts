// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

declare var firebase :any;

var This:any;

const {ccclass, property} = cc._decorator;



@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;


    alertBlock : cc.Node = null;
    userNameText : cc.EditBox;
    emailText: cc.EditBox;
    passwordText: cc.EditBox;
    confirmPasswordText: cc.EditBox;
    emterBtn:cc.Node = null;
    backBtn:cc.Node = null;



    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initChildNode();
        This = this;
    }
    
    start () {
        this.emterBtn.on(cc.Node.EventType.MOUSE_UP, this.onEnterPressed, this);
        this.backBtn.on(cc.Node.EventType.MOUSE_UP, this.onBackPressed, this);       
    }

    // update (dt) {

    // }

    initChildNode(){
        this.userNameText = this.node.getChildByName("userName").getComponent(cc.EditBox);
        this.emailText = this.node.getChildByName("email").getComponent(cc.EditBox);
        this.passwordText = this.node.getChildByName("passward").getComponent(cc.EditBox);
        this.confirmPasswordText = this.node.getChildByName("confirmPassward").getComponent(cc.EditBox);
        this.emterBtn = cc.find("bottomBtns/emterBtn", this.node);
        this.backBtn = cc.find("bottomBtns/backBtn", this.node);
        this.alertBlock = cc.find("title/alertBlock", this.node);
    }

    userPrompt(msg:string){ // display a board and a msg on the top
        var prompt = "<color=#f2daa2>" + msg + "</c>";
        this.alertBlock.getChildByName("richtext").getComponent(cc.RichText).string = prompt;
        this.alertBlock.opacity = 255;
        this.scheduleOnce(function() {
            this.alertBlock.runAction(cc.fadeOut(0.8));
        },3.5); 
    }

    onEnterPressed(event){
        // this.userPrompt("test test test test test test test test test test test test test test test test test test test test test test test");
        // return;
        // cc.log(this.userNameText.string);
        // cc.log(this.emailText.string);
        // cc.log(this.passwordText.string);
        // cc.log(this.confirmPasswordText.string);
        if(this.userNameText.string == ""){
            this.userPrompt("please input the user name~");
            return;
        }
        if(this.emailText.string == ""){
            this.userPrompt("please input the email~");
            return;
        }
        if(this.passwordText.string == ""){
            this.userPrompt("please input the passward~");
            return;
        }
        if(this.confirmPasswordText.string == ""){
            this.userPrompt("please input the confirm passward~");
            return;
        }
        if(this.confirmPasswordText.string != this.passwordText.string){
            this.userPrompt("confirm passward and passward are different!");
            this.passwordText.string = "";
            this.confirmPasswordText.string = "";
            return;
        }


        this.userPrompt("loading...");        
        firebase.auth().createUserWithEmailAndPassword(this.emailText.string,this.passwordText.string).then(function(result) {
            This.userPrompt("login success!");
            firebase.auth().onAuthStateChanged(function(user) {
                if(user) {
                    var userID = user.uid;
                    var userNameRef = firebase.database().ref(`${userID}/userName`);
                    // var userLifeRef = firebase.database().ref(`${userID}/Life`);
                    // var userCoinRef = firebase.database().ref(`${userID}/Coin`);
                    // var userScoreRef = firebase.database().ref(`${userID}/Score`);
                    // cc.log(userNameVariable);
                    userNameRef.set(This.userNameText.string).then(()=>{
                        This.userNameText.string = "";
                        This.emailText.string = "";
                        This.passwordText.string = "";
                        This.confirmPasswordText.string = "";
                    });
                    // userLifeRef.set(5);
                    // userCoinRef.set(0);
                    // userScoreRef.set(0);
                }
            })
        }).catch(function(error) {
            This.userNameText.string = "";
            This.emailText.string = "";
            This.passwordText.string = "";
            This.confirmPasswordText.string = "";
            var errorMessage = error.message;
            This.userPrompt(errorMessage);
            cc.log(errorMessage);
        });

        

    }

    onBackPressed(event){
        cc.log("back!");
        cc.director.loadScene("Game");  
    }
}
