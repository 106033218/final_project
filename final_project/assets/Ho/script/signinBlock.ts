// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

declare var firebase :any;
var This:any;


const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    

    alertBlock : cc.Node = null;
    emailText: cc.EditBox;
    passwordText: cc.EditBox;
    emterBtn:cc.Node = null;
    backBtn:cc.Node = null;
    userNameRef:string = "";

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initChildNode();
        This = this;
    }

    start () {
        this.emterBtn.on(cc.Node.EventType.MOUSE_UP, this.onEnterPressed, this);
        this.backBtn.on(cc.Node.EventType.MOUSE_UP, this.onBackPressed, this);
    }

    // update (dt) {}

    initChildNode(){
        this.emailText = this.node.getChildByName("email").getComponent(cc.EditBox);
        this.passwordText = this.node.getChildByName("passward").getComponent(cc.EditBox);
        this.emterBtn = cc.find("bottomBtns/emterBtn", this.node);
        this.backBtn = cc.find("bottomBtns/backBtn", this.node);
        this.alertBlock = cc.find("title/alertBlock", this.node);
    }

    userPrompt(msg:string){ // display a board and a msg on the top
        var prompt = "<color=#f2daa2>" + msg + "</c>";
        this.alertBlock.getChildByName("richtext").getComponent(cc.RichText).string = prompt;
        this.alertBlock.opacity = 255;
        this.scheduleOnce(function() {
            this.alertBlock.runAction(cc.fadeOut(0.8));
        },3.5); 
    }

    userLongPrompt(msg:string){ // display a board and a msg on the top
        var prompt = "<color=#f2daa2>" + msg + "</c>";
        this.alertBlock.getChildByName("richtext").getComponent(cc.RichText).string = prompt;
        this.alertBlock.opacity = 255;
        this.scheduleOnce(function() {
            this.alertBlock.runAction(cc.fadeOut(0.8));
        },50); 
    }

    onEnterPressed(event){
        this.userLongPrompt("loading...");
        firebase.auth().signInWithEmailAndPassword(this.emailText.string,this.passwordText.string).then(function() {
            This.getUid().then((msg)=>{
                // cc.log(msg);
                This.userNameRef = firebase.database().ref(`${msg}/userName`);
            
            }).then(()=>{
                This.userNameRef.once("value",(snapshot)=>{
                    if(snapshot.exists()){
                        // console.log(snapshot.val());
                        var string = "Hi " + snapshot.val() + "!";
                        This.userPrompt(string);
                    }
                    else{
                        console.log("error!");
                    }
                });
                
            }).then(()=>{
                This.emailText.string = "";
                This.passwordText.string = "";
                This.scheduleOnce(()=>{
                    cc.director.loadScene("Home");  
                }, 1);
            })

            // cc.director.loadScene("levelSelect"); //######################change scene
        }).catch(function(error) {
            // Handle Errors here.
            This.emailText.string = "";
            This.passwordText.string = "";
            var errorMessage = error.message;
            This.userPrompt(errorMessage);
        });

    }

    onBackPressed(event){
        cc.log("back!");
        cc.director.loadScene("Game");  
    }

    getUid(){
        return new Promise((resolve)=>{
            firebase.auth().onAuthStateChanged(function(user) {
                if(user){
                    resolve(user.uid);
                }
                else{
                    alert("please login first!");
                }
            });
        });
    }
}






