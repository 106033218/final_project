// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';


    private RDown: boolean = false;
    private LDown: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start () {

    }

    update (dt) {
        if(this.RDown){
            this.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
        }
        else if(this.LDown){
            this.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0);
        }
        else{
            this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
        }
    }

    onKeyDown(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.right:    
                this.RDown = true;
                break;
            case cc.macro.KEY.left:    //向左走
                this.LDown = true;
                break;
            
                
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.right:
                this.RDown = false;
                break;
            case cc.macro.KEY.left:
                this.LDown = false;
                break;
        }
    }

}
