// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    private userPrompt:cc.Node = null;
    private player:cc.Node = null;
    private playerIn:boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initChildNode();
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start () {

    }

    // update (dt) {}

    initChildNode(){
        this.userPrompt = this.node.getChildByName("userPrompt");
        this.userPrompt.opacity = 0;
    }

    onCollisionEnter(other, self) {
        cc.log("onCollisionEnter");
        if(other.node.name == "player"){
            this.userPrompt.opacity = 240;
            this.player = other.node;
            this.playerIn = true;
        }
    }


    onCollisionExit(other, self) {
        cc.log("onCollisionExit");
        if(other.node.name == "player"){
            this.userPrompt.opacity = 0;
            this.playerIn = false;
        }
    }

    onKeyDown(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.u:    
                break;

            
                
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.w:
                cc.log("w");
                if(this.playerIn){
                    this.move();
                }
                

            
                break;

            case cc.macro.KEY.q:
                // var state = "";
                // state +=  "playerIn:";
                // state += this.playerIn;
                // cc.log(state);
                // state = "";
                // state += "monsterBoardNode active:";
                // state += this.monsterBoardNode.active;
                // cc.log(state);
                // cc.log(this.player.y);
                break;




            

            
        }
    }

    move(){
        this.player.y += 383*2;
    }
}
