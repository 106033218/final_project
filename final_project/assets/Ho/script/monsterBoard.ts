// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html



const {ccclass, property} = cc._decorator;

@ccclass
export class monsterBoard extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;


    @property
    text: string = 'hello';


    private monsterId:number = 1;
    private bat:cc.Node = null;
    private bee:cc.Node = null;
    private slim:cc.Node = null;
    private spider:cc.Node = null;
    private batBtn:cc.Node = null;
    private beeBtn:cc.Node = null;
    private slimBtn:cc.Node = null;
    private spiderBtn:cc.Node = null;


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.initChildNode();
    }

    start () {
        this.batBtn.on(cc.Node.EventType.MOUSE_UP, this.onbatBtnClick , this);
        this.beeBtn.on(cc.Node.EventType.MOUSE_UP, this.onbeeBtnClick , this);
        this.slimBtn.on(cc.Node.EventType.MOUSE_UP, this.onslimBtnClick , this);
        this.spiderBtn.on(cc.Node.EventType.MOUSE_UP, this.onspidertBtnClick , this);
    }

    update (dt) {

    }

    initChildNode(){
        this.bat = cc.find("bat", this.node);
        this.bee = cc.find("bee", this.node);
        this.slim = cc.find("slim", this.node);
        this.spider = cc.find("spider", this.node);
        this.batBtn = cc.find("emterBtn", this.bat);
        this.beeBtn = cc.find("emterBtn", this.bee);
        this.slimBtn = cc.find("emterBtn", this.slim);
        this.spiderBtn = cc.find("emterBtn", this.spider);
        this.bee.color = new cc.Color(150, 150, 150);
        this.slim.color = new cc.Color(150, 150, 150);
        this.spider.color = new cc.Color(150, 150, 150);
    }

    // onKeyDown(event)
    // {
    //     switch(event.keyCode)
    //     {
    //         case cc.macro.KEY.u:    
    //             break;

            
                
    //     }
    // }

    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.up:
                switch(this.monsterId){
                    case 1: //bat
                        break;
                    case 2: //bee
                        this.onslimBtnClick();
                        break;
                    case 3://slim
                        this.onspidertBtnClick();
                        break;
                    case 4://spider
                        this.onbatBtnClick();
                        break;
                    default:
                        cc.log("???");
                        break;
                }

                break;
            case cc.macro.KEY.down:
                // cc.log("u");
                switch(this.monsterId){
                    case 1: //bat
                        this.onspidertBtnClick();
                        break;
                    case 2: //bee
                        break;
                    case 3: //slim
                        this.onbeeBtnClick();
                        break;
                    case 4: //spider
                        this.onslimBtnClick();
                        break;
                    default:
                        cc.log("???");
                        break;
                }

                break;
        }
    }

    onbatBtnClick(){
        // cc.log("onbatBtnClick");
        this.monsterId = 1;
        this.bat.color = new cc.Color(255, 255, 255);
        this.bee.color = new cc.Color(150, 150, 150);
        this.slim.color = new cc.Color(150, 150, 150);
        this.spider.color = new cc.Color(150, 150, 150);
    }

    onbeeBtnClick(){
        // cc.log("onbeeBtnClick");
        this.monsterId = 2;
        this.bat.color = new cc.Color(150, 150, 150);
        this.bee.color = new cc.Color(255, 255, 255);
        this.slim.color = new cc.Color(150, 150, 150);
        this.spider.color = new cc.Color(150, 150, 150);
    }
    onslimBtnClick(){

        // cc.log("onslimBtnClick");
        this.monsterId = 3;
        this.bat.color = new cc.Color(150, 150, 150);
        this.bee.color = new cc.Color(150, 150, 150);
        this.slim.color = new cc.Color(255, 255, 255);
        this.spider.color = new cc.Color(150, 150, 150);
    }
    onspidertBtnClick(){

        // cc.log("onspidertBtnClick");
        this.monsterId = 4;
        this.bat.color = new cc.Color(150, 150, 150);
        this.slim.color = new cc.Color(150, 150, 150);
        this.bee.color = new cc.Color(150, 150, 150);
        this.spider.color = new cc.Color(255, 255, 255);
    }

    returnMonsterId(){
        return this.monsterId;
    }

}