// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    player: cc.Node = null;

    @property(cc.Label)
    showtime: cc.Label = null;

    @property(cc.Node)
    g1: cc.Node = null;

    @property(cc.Node)
    g2: cc.Node = null;

    @property(cc.Node)
    g3: cc.Node = null;

    @property(cc.Prefab)
    skeleton: cc.Prefab = null;

    @property(cc.Prefab)
    shield: cc.Prefab = null;

    @property(cc.Prefab)
    spear: cc.Prefab = null;

    @property(cc.Prefab)
    boss: cc.Prefab = null;

    private time: number = 30;
    private win: boolean = false;
    private lose: boolean = false;

    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}

    start () {
        this.schedule(function(){
            
            if(this.time<=0){
                this.win = true;
                this.scheduleOnce(function(){
                    cc.director.loadScene("Win");
                },3)
            }
            else if(this.time%10==0){
                cc.log("monster");
                var monster: number; 
                var gate: number;
                monster = Math.floor(Math.random()*4-0.01);
                gate = Math.floor(Math.random()*3-0.01)
                // do{monster = Math.floor(Math.random()*4-0.01)} while(monster<0||monster>3) 
                // do{gate = Math.floor(Math.random()*3-0.01)} while(monster<0||monster>2) 
                // cc.log(monster, gate, 55555555555555555555);
                this.generate_monster(monster, gate);
            
            }
            if(this.time>0) this.time--;
        },1)
    }

    generate_monster(monsterId, gateId){
        var string = "generate_monster  " + monsterId;
        // cc.log(string);
        var monster;
        var gate;
        switch(monsterId){
            case 0:
                monster = cc.instantiate(this.skeleton);
                break;
            case 1:
                monster = cc.instantiate(this.shield);
                break;
            case 2:
                monster = cc.instantiate(this.spear);
                break;
            case 3:
                monster = cc.instantiate(this.boss);
                break;
            default:
                cc.log("something wrong");
        }
        switch(gateId){
            case 0:
                gate = this.g1;
                break;
            case 1:
                gate = this.g2;
                break;
            case 2:
                gate = this.g3;
                break;
        }
        cc.log(monster, gate);
        monster.position = gate.position;
        cc.log(monster.position, gate.position);
        this.node.addChild(monster);
    }

    update (dt) {
        this.showtime.getComponent(cc.Label).string = this.time.toString();
        if(this.player.getComponent("playerTest_n").dead || this.lose){
            cc.log("lose");
            this.lose = true;
            this.scheduleOnce(function(){
                cc.director.loadScene("Game_over");
            },3)
        }
    }
}
