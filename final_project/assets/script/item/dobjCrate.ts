// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import { gameMgr } from "../gameMgr";
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // @property(cc.Label)
    // label: cc.Label = null;
    @property(gameMgr)/**/
    gameMgr: gameMgr;

    private anim: cc.Animation = null;
    private break: boolean = false;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.anim = this.getComponent(cc.Animation);
    }

    // update (dt) {
    // }

    boxBreak(){
        this.break = true;
        if(!this.anim.getAnimationState("break").isPlaying) this.anim.play("break");
        this.anim.once('finished',function(){
            this.node.active = false;
        }, this)
    }   

    onCollisionEnter(other, self) {
        this.boxBreak();
    }
}
