// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { gameMgr } from "../gameMgr";

const {ccclass, property} = cc._decorator;
enum status_list    //用三種狀態去決定目前要播哪個動畫
{
  run = 1,
  at = 2,
  dead = 3
}

@ccclass
export default class NewClass extends cc.Component {
    @property()
    moveSpeed: number = 200;                 //角色水平移動速度 ************ 各角色不同

    @property(gameMgr)/**/
    gameMgr: gameMgr;

    private anim: cc.Animation = null;

    private moveDir = 0;                    //移動方向
    private grounded: boolean = false;      //是否落地
    private velocity: cc.RigidBody = null;  //速度控制
    private status: number = 1;             //角色狀態
    private at_box = null;                  //角色攻擊的碰撞格 ************
    private damage: number = 0              //所受傷害
    private player = null;                  //玩家節點
    private dead: boolean = false;          //角色是否死亡
    private maxHp: number = 500;            //最大生命值 ******************
    private hpBar = null;                   //血條
    private find_box = null;
    private find_player: boolean = false;
    private exp: number = 0;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.at_box = this.node.getChildByName("at");
        // this.player = cc.find("Canvas/game/player");
        this.hpBar = this.node.getChildByName("HP");
        this.find_box = this.node.getChildByName("find");
        this.gameMgr = cc.find("Canvas/UI").getComponent("gameMgr");
        let a = this.gameMgr.getEnemyTable();
        this.maxHp = a[2].hp;                               //////////////////////////////
        this.exp = a[1].exp;
        cc.log(this.gameMgr, 55555);    
    }

    start () {
        this.anim = this.getComponent(cc.Animation);
        this.velocity = this.getComponent(cc.RigidBody);
    }

    update (dt) {
        this.hpBar.scaleX = (this.maxHp-this.damage)/this.maxHp/10;
        if(!this.dead) this.find_box.active = !this.find_box.active;
        if(this.status == status_list.run){
            if(this.find_player){
                if(Math.abs(this.node.position.x-this.player.position.x)<50){           //當與玩家距離小於50
                    this.velocity.linearVelocity = cc.v2(0, this.velocity.linearVelocity.y);
                    this.moveDir = 0;
                    this.status = status_list.at;                                       //切換為攻擊狀態
                } 
                else {
                    this.moveDir = (this.node.position.x < this.player.position.x) ?1 :-1 ;
                    this.status = status_list.run;                                      //距離大於等於50就追玩家
                }
            }
            else{
                this.moveDir = -0.5;
            }
            this.velocity.linearVelocity = cc.v2(this.moveSpeed*this.moveDir, this.velocity.linearVelocity.y);  //角色在run時移動狀態
            if(this.moveDir!=0) this.node.scaleX = (this.moveDir > 0) ? 1 : -1;     //決定角色方向
        }
        else{
            if(!this.grounded)
                this.velocity.linearVelocity = cc.v2(this.velocity.linearVelocity.x, this.velocity.linearVelocity.y)    //空中行動無法控制水平速度
            else
                this.velocity.linearVelocity = cc.v2(0, this.velocity.linearVelocity.y)                                 //攻擊或死亡不移動
        }
        this.Animation();       //播放動畫
    }
    
    Animation(){
        switch(this.status){
            case status_list.run:
                if(this.moveDir==0){
                    if(!this.anim.getAnimationState("idle").isPlaying)
                    this.anim.play("idle");
                }
                else{
                    if(!this.anim.getAnimationState("run").isPlaying)
                        this.anim.play("run");
                }
                break;
            case status_list.at:
                if(!this.anim.getAnimationState("attack").isPlaying)
                this.anim.play("attack");
                break;
            case status_list.dead:
                if(!this.dead){
                    this.anim.play("die");
                    this.dead = true;
                }
                   
                break;
            default:
                break;
        }
    }

    Move(moveDir: number){    
        this.moveDir = moveDir;
    }

    Hurt(level){                 //角色受傷
        let a = this.gameMgr.getCurAtk();
        this.damage += a //被擊中傷害增加
        if(this.damage>=this.maxHp){
            this.damage = this.maxHp;
            this.status = status_list.dead;
            this.at_box.active = false;
            this.find_box.active = false;
            this.anim.once('finished',function(){       //當死亡動畫播完，角色節點消失
                this.scheduleOnce(function(){
                    this.node.active = false;
                    this.gameMgr.itemCreateByDieNode(this.node);/**/
                    this.gameMgr.addExpBy(this.exp);
                },2);
            }, this);
        } 
        var blink = cc.repeat(cc.sequence(cc.hide(),cc.delayTime(0.1),cc.show(),cc.delayTime(0.1)),3);  //被擊中閃爍
        this.node.runAction(blink);
    }

    attack(){                                   //這個要用關鍵幀來調用函數，從動畫窗格新增在角色把武器砍出去那幀
        this.at_box.active = true;
        // this.gameMgr.addHpBy(-5); /**/
        this.anim.once('finished',function(){   //攻擊動畫結束時回到run狀態，取消攻擊碰種格
            if(!this.dead) this.status = status_list.run;
            this.at_box.active = false;
        }, this);
    }

    onBeginContact(contact, selfCollider, otherCollider){
        let a = contact.getWorldManifold().normal.y;
        if(a == -1) this.grounded = true;
    }

    onEndContact(contact, selfCollider, otherCollider){
        if(this.velocity.linearVelocity.y!=0) this.grounded = false;
    }

    onCollisionEnter(other, self) {
        cc.log(other.node.name, "in");
        if(other.node.name=="enemyFind"){
            this.player = other.node.parent;
            this.find_player = true;
            cc.log(this.player);
        }
        else if(other.node.name=="doorTop" || other.node.name=="doorModdle" || other.node.name=="doorBottom"){
            this.node.parent.getComponent("enemy_system").lose = true;
            cc.log(this.node.parent.getComponent("enemy_system").lose, 66666666666666666);
        }
        else{
            if(!this.dead) this.Hurt(2);
        }
    }

    onCollisionExit(other, self) {
        cc.log(other.node.name, "out");
        if(other.node.name=="enemyFind"){
            this.player = null;
            this.find_player = false;
        }
        // cc.log(this.findTarget,2);
    }
}
