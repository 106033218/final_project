// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

// import { gameMgr } from "./gameMgr";

const { ccclass, property } = cc._decorator;

enum status_list {
    //   idle = 0,
    run = 1,
    roll = 2,
    at_1 = 3,
    at_2 = 4,
    at_3 = 5,
    jump = 6,
    dead = 7
}

@ccclass
export default class NewClass extends cc.Component {

    @property()
    playerSpeed: number = 500;

    @property(cc.Node)/**/
    UI: cc.Node = null;

    @property(cc.Node)
    camara: cc.Node = null;

    private anim: cc.Animation = null;

    private moveDir = 0;

    private velocity: cc.RigidBody = null;

    private aDown: boolean = false;
    private dDown: boolean = false;
    private jDown: boolean = false;
    private kDown: boolean = false;
    private lDown: boolean = false;
    private wDown: boolean = false;
    private spaceDown: boolean = false;
    private status: number = 6;
    private grounded: boolean = false;
    private combo: number = 0;
    dead: boolean = false;
    private roll: boolean = false;
    private at_box = null;
    private at_1_box = null;
    private at_2_box = null;
    private at_3_box = null;
    private wallJump: boolean = false;
    private damage: number = 0;
    private gameMgr = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.at_box = this.node.getChildByName("at");
        this.at_1_box = this.at_box.getChildByName("at_1");;
        this.at_2_box = this.at_box.getChildByName("at_2");
        this.at_3_box = this.at_box.getChildByName("at_3");
        this.gameMgr = this.UI.getComponent("gameMgr");
        cc.log(this.gameMgr, 6666);
    }

    start() {
        this.anim = this.getComponent(cc.Animation);
        this.velocity = this.getComponent(cc.RigidBody);
    }

    update(dt) {
        // if(this.moveDir!=0) this.node.scaleX = (this.moveDir > 0) ? 1 : -1;
        // this.grounded = (this.velocity.linearVelocity.y!=0 || (this.velocity.linearVelocity.y==0 && this.status==status_list.jump)) ? false : true;
        if(!this.aDown && !this.dDown){
            this.wallJump = false;
        }
        if (this.status == status_list.run || this.status == status_list.jump) {
            if (this.moveDir != 0) this.node.scaleX = (this.moveDir > 0) ? 1 : -1;
            this.status = (this.grounded) ? status_list.run : status_list.jump;
            this.velocity.linearVelocity = cc.v2(this.playerSpeed * this.moveDir, this.velocity.linearVelocity.y);
        }
        else {
            if (!this.grounded)
                this.velocity.linearVelocity = cc.v2(this.velocity.linearVelocity.x, this.velocity.linearVelocity.y)
            else
                this.velocity.linearVelocity = cc.v2(0, this.velocity.linearVelocity.y)
        }

        this.playerAnimation();
        this.camara.x = this.node.x;
        this.camara.y = this.node.y+300;
        this.UI.x = this.node.x - 960;
        this.UI.y = this.node.y - 230;
    }

    playerAnimation() {
        // cc.log(this.status);
        switch (this.status) {
            case status_list.run:
                if (this.moveDir == 0) {
                    if (!this.anim.getAnimationState("idle").isPlaying)
                        this.anim.play("idle");
                }
                else {
                    if (!this.anim.getAnimationState("run").isPlaying)
                        this.anim.play("run");
                }
                break;

            case status_list.jump:
                if (this.velocity.linearVelocity.y > 0) {
                    if (!this.anim.getAnimationState("rise").isPlaying)
                        this.anim.play("rise");
                }
                else {
                    if (!this.anim.getAnimationState("fall").isPlaying)
                        this.anim.play("fall");
                }
                break;

            case status_list.roll:

                if (!this.anim.getAnimationState("roll").isPlaying)
                    this.anim.play("roll");
                break;

            case status_list.at_1:
                if (!this.anim.getAnimationState("attack_1").isPlaying)
                    this.anim.play("attack_1");
                break;

            case status_list.at_2:
                if (!this.anim.getAnimationState("attack_2").isPlaying)
                    this.anim.play("attack_2");
                break;

            case status_list.at_3:
                if (!this.anim.getAnimationState("attack_3").isPlaying)
                    this.anim.play("attack_3");
                break;
            case status_list.dead:
                if (!this.dead) {
                    this.anim.play("die");
                    this.dead = true;
                }
                break;
            default:
                break;

        }

    }

    onKeyDown(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.a:    //向左走
                this.aDown = true;
                this.playerMove(-1);
                break;
            case cc.macro.KEY.d:    //向右走
                this.dDown = true;
                this.playerMove(1);
                break;
            case cc.macro.KEY.k:    //跳躍
                this.kDown = true;
                this.playerJump(1200);
                break;
            case cc.macro.KEY.l:    //翻滾
                this.lDown = true;
                this.playerRoll();
                break;
            case cc.macro.KEY.j:    //攻擊
                this.jDown = true;
                if (this.status == status_list.run || this.status == status_list.jump)
                    switch (this.combo) {
                        case 0:
                            this.status = status_list.at_1;
                            break;
                        case 1:
                            this.status = status_list.at_2;
                            break;
                        case 2:
                            this.status = status_list.at_3;
                            break;
                    }
                break;
            case cc.macro.KEY.num1:
                this.gameMgr.itemUse(1)
                break;
            case cc.macro.KEY.num2:
                this.gameMgr.itemUse(2)
                break;
            case cc.macro.KEY.num3:
                this.gameMgr.itemUse(3)
                break;
            case cc.macro.KEY.num4:
                this.gameMgr.itemUse(4)
                break;
            case cc.macro.KEY.num5:
                this.gameMgr.itemUse(5)
                break;
            case cc.macro.KEY.num6:
                this.gameMgr.itemUse(6)
                break;
            case cc.macro.KEY.num7:
                this.gameMgr.getSts(0);
                break;
            case cc.macro.KEY.num8:
                this.gameMgr.addExpBy(100);
                break;
        
          

        }
    }

    onKeyUp(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                this.aDown = false;
                if (this.dDown)
                    this.playerMove(1);
                else
                    this.playerMove(0);
                break;
            case cc.macro.KEY.d:
                this.dDown = false;
                if (this.aDown)
                    this.playerMove(-1);
                else
                    this.playerMove(0);
                break;
            case cc.macro.KEY.k:
                this.wDown = false;
                break;
            case cc.macro.KEY.l:
                this.lDown = false;
                break;
            case cc.macro.KEY.j:
                this.jDown = false;
                break;
        }
    }

    playerMove(moveDir: number) {
        this.moveDir = moveDir;
    }

    playerJump(y_velocity: number) {
        if (this.status == status_list.run) {
            cc.loader.loadRes("./audio/seJump", cc.AudioClip, null, function (err, clip) {
                cc.audioEngine.playEffect(clip, false);
            });
            this.velocity.linearVelocity = cc.v2(this.velocity.linearVelocity.x, y_velocity);
            // this.grounded = false;
            // this.status = status_list.jump;
            // cc.audioEngine.playEffect(this.jumpSound, false);
        }
        else if (this.status == status_list.jump) {
            if (this.wallJump) {
                cc.loader.loadRes("./audio/seJump", cc.AudioClip, null, function (err, clip) {
                    cc.audioEngine.playEffect(clip, false);
                });
                this.playerSpeed = -500;
                this.velocity.linearVelocity = cc.v2(this.velocity.linearVelocity.x, y_velocity);
                this.scheduleOnce(function () {
                    this.playerSpeed = 500;
                }, 0.1)
            }

        }
    }

    playerRoll() {
        if (this.status == status_list.run && this.roll == false) {
            this.status = status_list.roll;
            this.roll = true;
            this.scheduleOnce(function () {
                this.roll = false;
            }, 1);
            this.anim.once('finished', function () {
                this.status = status_list.run;
            }, this);
            let a = (this.node.scaleX > 0) ? 1 : -1;
            let action = cc.moveBy(0.4, a * 300, 0);
            this.node.runAction(action);
        }
    }

    playerAttack() {
        cc.loader.loadRes("./audio/seAtk", cc.AudioClip, null, function (err, clip) {
            cc.audioEngine.playEffect(clip, false);
        });
        switch (this.combo) {
            case 0:
                // this.status = status_list.at_1;
                this.combo++;
                this.at_1_box.active = true;
                this.scheduleOnce(function () {
                    // cc.log("hi");
                    if (this.combo == 1) this.combo = 0;
                }, 1.5);
                this.anim.once('finished', function () {
                    if (!this.dead) {
                        this.status = (this.grounded) ? status_list.run : status_list.jump;
                        cc.log("at_1", this.status, this.combo);
                    }

                    this.at_1_box.active = false;
                }, this);
                break;
            case 1:
                // this.status = status_list.at_2;
                this.combo++;
                this.at_2_box.active = true;
                this.scheduleOnce(function () {
                    // cc.log("ho");
                    if (this.combo == 2) this.combo = 0;
                }, 1.5);
                this.anim.once('finished', function () {
                    if (!this.dead) {
                        this.status = (this.grounded) ? status_list.run : status_list.jump;
                        cc.log("at_2", this.status, this.combo);
                    }
                    this.at_2_box.active = false;
                }, this);
                break;
            case 2:
                // this.status = status_list.at_3;
                this.combo = 0;
                this.at_3_box.active = true;
                this.anim.once('finished', function () {
                    if (!this.dead) {
                        this.status = (this.grounded) ? status_list.run : status_list.jump;
                        cc.log("at_3", this.status, this.combo);
                    }
                    this.at_3_box.active = false;
                }, this);
                break;
            default:
                break;
        }


    }

    playerHurt(atk) {
        // this.damage++;
        if(this.gameMgr.addHpBy(-atk)<=0){
            this.status = status_list.dead;
            this.at_box.active = false;
            this.anim.once('finished', function () {       //當死亡動畫播完，角色節點消失
                this.scheduleOnce(function () {
                    this.node.active = false;
                }, 2);
            }, this);
        } /**/
        // if (this.damage > 5) {
        //     this.status = status_list.dead;
        //     this.at_box.active = false;
        //     this.anim.once('finished', function () {       //當死亡動畫播完，角色節點消失
        //         this.scheduleOnce(function () {
        //             this.node.active = false;
        //         }, 2);
        //     }, this);
        // }
        var blink = cc.repeat(cc.sequence(cc.hide(), cc.delayTime(0.1), cc.show(), cc.delayTime(0.1)), 3);
        this.node.runAction(blink);
    }

    onBeginContact(contact, selfCollider, otherCollider) {
        let a = contact.getWorldManifold().normal.y;
        let b = contact.getWorldManifold().normal.x;
        cc.log(otherCollider.node.name, a, b);
        // if(a == -1&&this.status==status_list.jump) this.status = status_list.run;
        if(otherCollider.node.parent.name == "Platform" && a>=-0.9){
            contact.disabled = true;
        }
        else{
            if (a < -0.9) this.grounded = true;

            if (b >0.9 || b < -0.9) this.wallJump = true;
    
            // if (Math.floor(otherCollider.tag / 100) == 3) {/**/
            //     // this.gameMgr.itemPickUp(otherCollider.tag % 300);
            //     otherCollider.node.destroy();
            // }
            if (otherCollider.name.slice(0, 4)=="item") {/**/
                this.gameMgr.itemPickUpByNode(otherCollider.node);
            }
        }
        
    }

    onEndContact(contact, selfCollider, otherCollider) {
        let b = contact.getWorldManifold().normal.x;
        let a = contact.getWorldManifold().normal.y;
        cc.log(otherCollider.node.name,b,a);
        // if(a == -1&&this.status==status_list.jump) this.status = status_list.run;
        if(Math.abs(this.velocity.linearVelocity.y)>200) this.grounded = false;
            if(Math.abs(this.velocity.linearVelocity.x)>10){
                this.wallJump = false; 
                cc.log(this.wallJump);
            } 
        

    }

    onCollisionEnter(other, self) {
        if(other.tag==87) return;
        // cc.log(self.tag);
        if (!this.dead && !this.roll){
            let a = this.gameMgr.getEnemyTable();

            if(other.node.parent.name=="at"){
                var name = other.node.parent.parent.name;
                // cc.log(name,123456789);
                for(var i=0;i<6;i++){
                    cc.log(a[i].name,987654321);
                    if(name == a[i].name){
                        this.playerHurt(a[i].atk);
                        break;
                    }
                }
            }
            else{
                var name = other.node.parent.name;
                // cc.log(name,123456789);
                for(var i=0;i<6;i++){
                    // cc.log(a[i].name,987654321);
                    if(name == a[i].name){
                        this.playerHurt(a[i].atk);
                        break;
                    }
                }
            }
        }
    }
}
