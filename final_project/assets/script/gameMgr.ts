/*
    Use gameMgr:
    Step1: Copy the node "gameMgr" in "Yang/UI/UITest.fire" into your scene.
    Step2: In your script, add "import { gameMgr } from "./gameMgr";" (may differ) in front of "@ccclass"
           (When you enter "import gameMgr", it will automatically fill in the path.)
    Step3: Add this in the beginning of class:
            @property(gameMgr)
            gameMgr: gameMgr;
    Step4: Drag the node "gameMgr" into the "gameMgr" field in the component of your node. 
    Step5: Then you can use functions in gameMgr.
           e.g. this.gameMgr.addHpBy(10);
*/

const { ccclass, property } = cc._decorator;
@ccclass
export class gameMgr extends cc.Component {

    @property
    text: string = 'hello';

    @property({ type: cc.AudioClip })
    bgm: cc.AudioClip = null;

    private static levelTable = [
        {},
        {
            level: 1,
            atk: 35,
            maxHp: 200,
            maxExp: 100,
            maxGrid: 10,
            maxLoading: 50
        },
        {
            level: 2,
            atk: 50,
            maxHp: 280,
            maxExp: 150,
            maxGrid: 12,
            maxLoading: 65
        },
        {
            level: 3,
            atk: 60,
            maxHp: 350,
            maxExp: 210,
            maxGrid: 14,
            maxLoading: 80
        },
        {
            level: 4,
            atk: 70,
            maxHp: 400,
            maxExp: 280,
            maxGrid: 16,
            maxLoading: 95
        },
        {
            level: 5,
            atk: 80,
            maxHp: 450,
            maxExp: 360,
            maxGrid: 18,
            maxLoading: 110
        },
        {
            level: 6,
            atk: 90,
            maxHp: 490,
            maxExp: 450,
            maxGrid: 20,
            maxLoading: 120
        },
        {
            level: 7,
            atk: 95,
            maxHp: 530,
            maxExp: 550,
            maxGrid: 22,
            maxLoading: 130
        },
        {
            level: 8,
            atk: 100,
            maxHp: 565,
            maxExp: 660,
            maxGrid: 24,
            maxLoading: 135
        },
        {
            level: 9,
            atk: 105,
            maxHp: 600,
            maxExp: 780,
            maxGrid: 24,
            maxLoading: 140
        },
        {
            level: 10,
            atk: 110,
            maxHp: 630,
            maxExp: 999,
            maxGrid: 24,
            maxLoading: 145
        }
    ];

    private static itemTable = [
        {
            id: 0,
            nameEn: "itemCoin",
            photoURL: "./itemPhotos/itemCoin",
        }, {
            id: 1,
            nameCh: "生命藥水",
            nameEn: "itemPotionR",
            photoURL: "./itemPhotos/itemPotionR",
            info: "紅色光澤展現生命的力量，服用後HP+25。",
            price: 50,
            value: 25,
            loading: 2
        }, {
            id: 2,
            nameCh: "精華藥水",
            nameEn: "itemPotionY",
            photoURL: "./itemPhotos/itemPotionY",
            info: "黃色光澤象徵精華的萃取，服用後HP+100。",
            price: 150,
            value: 75,
            loading: 3
        }, {
            id: 3,
            nameCh: "森林藥水",
            nameEn: "itemPotionG",
            photoURL: "./itemPhotos/itemPotionG",
            info: "綠色光澤濃縮森林的祝福，服用後HP全滿。",
            price: 500,
            value: 250,
            loading: 5
        }, {
            id: 4,
            nameCh: "靈魂藥水",
            nameEn: "itemPotionB",
            photoURL: "./itemPhotos/itemPotionB",
            info: "藍色光澤激發靈魂的潛能，服用後在30秒內攻擊力+25%。",
            price: 1000,
            value: 500,
            loading: 10
        }, {
            id: 5,
            nameCh: "蘑菇",
            nameEn: "itemMushroom",
            photoURL: "./itemPhotos/itemMushroom",
            info: "路北森林裡清爽的蘑菇，服用後可解除中毒狀態。",
            value: 300,
            loading: 5
        }, {
            id: 6,
            nameCh: "翡翠蘑菇",
            nameEn: "itemMushroomUltra",
            photoURL: "./itemPhotos/itemMushroomUltra",
            info: "透出神秘色彩的翡翠蘑菇，服用後可在15秒內維持無敵狀態。",
            value: 500,
            loading: 10
        }, {
            id: 7,
            nameCh: "力量聖石",
            nameEn: "itemRockStr",
            photoURL: "./itemPhotos/itemRockStr",
            info: "象徵力量的聖石，似乎與基地升級或召喚有一定的關係。",
            value: 500,
            loading: 0
        }, {
            id: 8,
            nameCh: "敏捷聖石",
            nameEn: "itemRockDex",
            photoURL: "./itemPhotos/itemRockDex",
            info: "象徵敏捷的聖石，似乎與基地升級或召喚有一定的關係。",
            value: 500,
            loading: 0
        }, {
            id: 9,
            nameCh: "幸運聖石",
            nameEn: "itemRockLuk",
            photoURL: "./itemPhotos/itemRockLuk",
            info: "象徵幸運的聖石，似乎與基地升級或召喚有一定的關係。",
            value: 500,
            loading: 0
        }, {
            id: 10,
            nameCh: "智慧聖石",
            nameEn: "itemRockInt",
            photoURL: "./itemPhotos/itemRockInt",
            info: "象徵智慧的聖石，似乎與基地升級或召喚有一定的關係。",
            value: 500,
            loading: 0
        }, {
            id: 11,
            nameCh: "碎石",
            nameEn: "itemChip",
            photoURL: "./itemPhotos/itemChip",
            info: "普通的碎石。",
            value: 50,
            loading: 5
        }, {
            id: 12,
            nameCh: "力量碎石",
            nameEn: "itemChipStr",
            photoURL: "./itemPhotos/itemChipStr",
            info: "散發著某種神祕氣息的碎石，象徵著力量。",
            value: 75,
            loading: 2
        }, {
            id: 13,
            nameCh: "敏捷碎石",
            nameEn: "itemChipDex",
            photoURL: "./itemPhotos/itemChipDex",
            info: "散發著某種神祕氣息的碎石，象徵著敏捷。",
            value: 75,
            loading: 2
        }, {
            id: 14,
            nameCh: "幸運碎石",
            nameEn: "itemChipLuk",
            photoURL: "./itemPhotos/itemChipLuk",
            info: "散發著某種神祕氣息的碎石，象徵著幸運。",
            value: 75,
            loading: 2
        }, {
            id: 15,
            nameCh: "智慧碎石",
            nameEn: "itemChipInt",
            photoURL: "./itemPhotos/itemChipInt",
            info: "散發著某種神祕氣息的碎石，象徵著智慧。",
            value: 75,
            loading: 2
        }, {
            id: 16,
            nameCh: "骷髏殘骸",
            nameEn: "itemSkull",
            photoURL: "./itemPhotos/itemSkull",
            info: "骷髏的殘骸，透出紫色的微光，似乎有些不善。",
            value: 50,
            loading: 1
        }, {
            id: 17,
            nameCh: "詛咒木盾",
            nameEn: "itemShield",
            photoURL: "./itemPhotos/itemShield",
            info: "骷髏盾兵所掉出的木盾，透出紫色的微光，似乎有些不善。",
            value: 100,
            loading: 2
        }, {
            id: 18,
            nameCh: "詛咒長槍",
            nameEn: "itemSpear",
            photoURL: "./itemPhotos/itemSpear",
            info: "骷髏長槍手的長槍，透出紫色的微光，似乎有些不善。",
            value: 125,
            loading: 3
        }, {
            id: 19,
            nameCh: "詛咒之弓",
            nameEn: "itemBow",
            photoURL: "./itemPhotos/itemBow",
            info: "骷髏射手的弓，透出紫色的微光，似乎有些不善。",
            value: 150,
            loading: 3
        }, {
            id: 20,
            nameCh: "詛咒法杖",
            nameEn: "itemCane",
            photoURL: "./itemPhotos/itemCane",
            info: "骷髏法師的法杖，透出紫色的微光，似乎有些不善。",
            value: 175,
            loading: 3
        }, {
            id: 21,
            nameCh: "詛咒鐵盔",
            nameEn: "itemHelmet",
            photoURL: "./itemPhotos/itemHelmet",
            info: "骷髏王的鐵盔，透出紫色的微光，似乎有些不善。",
            value: 250,
            loading: 5
        }, {
            id: 22,
            nameCh: "詛咒巨斧",
            nameEn: "itemAx",
            photoURL: "./itemPhotos/itemAx",
            info: "骷髏王的巨斧，透出紫色的微光，似乎有些不善。",
            value: 300,
            loading: 10
        }, {
            id: 23,
            nameCh: "祕書殘頁",
            nameEn: "itemSheet",
            photoURL: "./itemPhotos/itemSheet",
            info: "上古祕書的殘頁，隱約看出好像與基地的升級有關。",
            value: 1,
            loading: 10
        }, {
            id: 24,
            nameCh: "殘破旗幟",
            nameEn: "itemBanner",
            photoURL: "./itemPhotos/itemBanner",
            info: "殘破不堪的旗幟，似乎與基地的升級有關。",
            value: 1,
            loading: 10
        }
    ];

    private static summonTable = [
        {
            id: 0,
            name: "Bat",
            atk: 50,
            smnItemIdx: 7
        },
        {
            id: 1,
            name: "Spider",
            atk: 60,
            smnItemIdx: 8
        },
        {
            id: 2,
            name: "Slime",
            atk: 80,
            smnItemIdx: 9
        },
        {
            id: 3,
            name: "Bee",
            atk: 100,
            smnItemIdx: 10
        }
    ]

    private static enemyTable = [
        {
            id: 0,
            name: "Skeleton",
            atk: 50,
            hp: 200,
            exp: 40,
            genItem: [
                [11, .4],
                [12, .4],
                [16, .4]
            ]
        },
        {
            id: 1,
            name: "Skeleton_Shield",
            atk: 40,
            hp: 350,
            exp: 60,
            genItem: [
                [11, .4],
                [13, .3],
                [16, .4],
                [17, .3]
            ]
        },
        {
            id: 2,
            name: "Skeleton_Spear",
            atk: 70,
            hp: 300,
            exp: 60,
            genItem: [
                [11, .4],
                [12, .4],
                [16, .4],
                [18, .3],
                [23, .2]
            ]
        },
        {
            id: 3,
            name: "Skeleton_Archer",
            atk: 30,
            hp: 200,
            exp: 80,
            genItem: [
                [11, .3],
                [13, .3],
                [16, .4],
                [19, .3],
                [23, .2]
            ]
        },
        {
            id: 4,
            name: "Skeleton_Mage",
            atk: 50,
            hp: 300,
            exp: 120,
            genItem: [
                [11, .3],
                [15, .2],
                [16, .4],
                [20, .3],
                [23, .2]
            ]
        },
        {
            id: 5,
            name: "Skeleton_Boss",
            atk: 60,
            hp: 500,
            exp: 300,
            genItem: [
                [7, .2],
                [8, .2],
                [9, .2],
                [10, .2],
                [14, .7],
                [15, .7],
                [16, .4],
                [21, .4],
                [22, .3],
                [24, .4]
            ]
        }
    ]

    // directly needed data from firebase
    private playerName = null;
    private curLevel = 1;
    private curHp = 0;
    private curExp = 0;
    private curSts = [];
    private curBpk = [];

    // indirectly computed data
    private curGrid = 0;
    private curLoading = 0;
    private curAtk = 0;
    private lastPanel = null;
    private curPanel = null;
    private bpkBgArray = [];
    private bpkArray = [];
    private visibleBpkBgArray = [];
    private visibleBpkArray = [];
    private pageOffset = 0;
    private lastSelectIdx = -1;
    private curSelectIdx = -1;

    start() {
        var lastInfo = cc.find("persistInfo").getComponent("persistInfo").getInfo();

        this.playerName = lastInfo.playerName;
        this.curLevel = lastInfo.lastLevel;
        this.curHp = lastInfo.lastHp;
        this.curExp = lastInfo.lastExp;
        this.curSts = lastInfo.lastSts;
        this.curBpk = lastInfo.lastBpk;
        this.curAtk = gameMgr.levelTable[this.curLevel].atk;

        for (var i = 1; i < this.curBpk.length; i++) { // 0 stores money, continue
            if (this.curBpk[i] > 0) {
                this.curGrid++;
                this.curLoading += gameMgr.itemTable[i].loading * this.curBpk[i];
            }
        }

        // status timer
        this.schedule(() => {
            for (var i = 0; i < 3; i++) {
                if (this.curSts[i] <= 0) this.cancelSts(i);
                else {
                    if (i == 0) this.addHpBy(-5);
                    this.curSts[i]--;
                }
            }
        }, 1);

        // BGM play
        if (this.bgm) { // if null, continue
            cc.log("123");
            cc.audioEngine.stopMusic();
            cc.audioEngine.playMusic(this.bgm, true);
        }

        // mapName
        cc.log(cc.find("Canvas/UI/uiMap/uiMapName").getComponent(cc.Label).string, this.text);
        cc.find("Canvas/UI/uiMap/uiMapName").getComponent(cc.Label).string = this.text;

        // For sorting bug
        this.itemDisplayDebug();
    }
    
    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    onKeyDown(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.num1:
                this.itemUse(1)
                break;
            case cc.macro.KEY.num2:
                this.itemUse(2)
                break;
            case cc.macro.KEY.num3:
                this.itemUse(3)
                break;
            case cc.macro.KEY.num4:
                this.itemUse(4)
                break;
            case cc.macro.KEY.num5:
                this.itemUse(5)
                break;
            case cc.macro.KEY.num6:
                this.itemUse(6)
                break;
            case cc.macro.KEY.num7:
                this.getSts(0);
                break;
            case cc.macro.KEY.num8:
                this.addExpBy(100);
                break;
        }
    }

    update() {
        var levelNode = cc.find("Canvas/UI/uiPlayer/uiPlayerInfo");
        var hpNode = cc.find("Canvas/UI/uiHp");
        var expNode = cc.find("Canvas/UI/uiExp");
        var stsBgNode = cc.find("Canvas/UI/uiStatus/stsBgGrid");
        var stsPsnBgNode = stsBgNode.getChildByName("psnBg");
        var stsAtkBgNode = stsBgNode.getChildByName("atkBg");
        var stsAvdBgNode = stsBgNode.getChildByName("avdBg");

        levelNode.getComponent(cc.Label).string = this.playerName + "（LV" + this.curLevel + "）";
        hpNode.getChildByName("uiHpBar").scaleX = this.curHp / gameMgr.levelTable[this.curLevel].maxHp;
        hpNode.getChildByName("uiHpInfo").getComponent(cc.Label).string = this.curHp + "/" + gameMgr.levelTable[this.curLevel].maxHp;
        expNode.getChildByName("uiExpBar").scaleX = this.curExp / gameMgr.levelTable[this.curLevel].maxExp;
        expNode.getChildByName("uiExpInfo").getComponent(cc.Label).string = this.curExp + "/" + gameMgr.levelTable[this.curLevel].maxExp;

        if (stsPsnBgNode) stsPsnBgNode.scaleX = this.curSts[0] / 10; // poison: 10s
        if (stsAtkBgNode) stsAtkBgNode.scaleX = this.curSts[1] / 30; // attack: 30s
        if (stsAvdBgNode) stsAvdBgNode.scaleX = this.curSts[2] / 15; // attack: 15s
    }

    public addHpBy(deltaHp: number) {

        if (this.curHp + deltaHp <= 0) {
            this.curHp = 0;
            return this.curHp;
        } else if (this.curHp + deltaHp >= gameMgr.levelTable[this.curLevel].maxHp) {
            this.curHp = gameMgr.levelTable[this.curLevel].maxHp;
        } else {
            this.curHp = this.curHp + deltaHp;
        }
    }

    public addExpBy(deltaExp: number) {
        if (this.curExp + deltaExp <= 0) {
            this.curExp = 0;
        } else if (this.curExp + deltaExp >= gameMgr.levelTable[this.curLevel].maxExp) {
            if (this.curLevel < 10) {
                this.curExp = this.curExp + deltaExp - gameMgr.levelTable[this.curLevel].maxExp;
                this.levelUp();
            } else {
                this.curExp = gameMgr.levelTable[this.curLevel].maxExp;
            }
        } else {
            this.curExp = this.curExp + deltaExp;
        }
    }

    public levelUp() {
        this.curLevel++;
        cc.loader.loadRes("./audio/seLevelUp", cc.AudioClip, null, function (err, clip) {
            cc.audioEngine.playEffect(clip, false);
        });
    }

    public getSts(stsIdx: number) {
        var stsNode = cc.find("Canvas/UI/uiStatus/stsGrid")
        var stsBgNode = cc.find("Canvas/UI/uiStatus/stsBgGrid")
        var stsPath = "";
        var stsName = "";

        switch (stsIdx) {
            case 0:
                this.curSts[0] = 10;
                stsPath = "./UIPhotos/uiStatusPoison"
                stsName = "psn";
                break;
            case 1:
                this.curSts[1] = 30;
                stsPath = "./UIPhotos/uiStatusAttack"
                stsName = "atk";
                this.curAtk *= 1.25;
                break;
            case 2:
                this.curSts[2] = 15;
                stsPath = "./UIPhotos/uiStatusAvoid"
                stsName = "avd";
                // TODO
                break;
        }

        if (stsNode.getChildByName(stsName)) return;

        cc.loader.loadRes(stsPath, cc.SpriteFrame, function (err, spriteFrame) {
            var newSts = new cc.Node(stsName);

            var sprite = newSts.addComponent(cc.Sprite);
            sprite.spriteFrame = spriteFrame;
            sprite.sizeMode = cc.Sprite.SizeMode.RAW;
            sprite.trim = false;

            newSts.parent = stsNode;
        });
        cc.loader.loadRes(stsPath + "Bg", cc.SpriteFrame, function (err, spriteFrame) {
            var newBg = new cc.Node(stsName + "Bg");
            newBg.anchorX = 0;

            var sprite = newBg.addComponent(cc.Sprite);
            sprite.spriteFrame = spriteFrame;
            sprite.sizeMode = cc.Sprite.SizeMode.RAW;
            sprite.trim = false;

            newBg.parent = stsBgNode;
        });
    }

    public cancelSts(stsIdx: number) {
        var stsNode = cc.find("Canvas/UI/uiStatus/stsGrid")
        var stsBgNode = cc.find("Canvas/UI/uiStatus/stsBgGrid")
        var stsName = "";

        switch (stsIdx) {
            case 0:
                this.curSts[0] = 0;
                stsName = "psn";
                break;
            case 1:
                this.curSts[1] = 0;
                stsName = "atk";
                if (this.curAtk > gameMgr.levelTable[this.curLevel].atk)
                    this.curAtk /= 1.25;
                break;
            case 2:
                this.curSts[2] = 0;
                stsName = "avd";
                break;
        }

        if (!stsNode.getChildByName(stsName)) return;

        stsNode.getChildByName(stsName).destroy();
        stsBgNode.getChildByName(stsName + "Bg").destroy();
    }

    public panelActiveMgr(e, panel: String) {
        
        cc.loader.loadRes("./audio/seClickBtn", cc.AudioClip, null, function (err, clip) {
            cc.audioEngine.playEffect(clip, false);
        });

        function getNode(panel) {
            switch (panel) {
                case "set":
                    return cc.find("Canvas/UI/uiSetting");
                case "bpk":
                    return cc.find("Canvas/UI/uiBackpack");
                case "rtn":
                    return cc.find("Canvas/UI/uiReturn");
                case "sel":
                    return cc.find("Canvas/UI/uiSell");
                case "buy":
                    return cc.find("Canvas/UI/uiBuy");
                case "cft":
                    return cc.find("Canvas/UI/uiCraft");
                case "wld":
                    return cc.find("Canvas/UI/uiWorld");
                case "bpkCfm":
                    return cc.find("Canvas/UI/uiBackpack/bpkConfirm");
                case "selCfm":
                    return cc.find("Canvas/UI/uiSell/selConfirm");
                case "buyCfm":
                    return cc.find("Canvas/UI/uiBuy/buyConfirm");
                case "cftCfm":
                    return cc.find("Canvas/UI/uiCraft/cftConfirm");
            }
        }

        this.curPanel = panel;

        if (panel == this.lastPanel && getNode(panel).active) {
            getNode(panel).active = false;
        } else { // closed same or others
            if (this.lastPanel && this.lastPanel + "Cfm" != panel) {
                // not first open, and lastPanel's cfm is not panel
                getNode(this.lastPanel).active = false;
                if (this.lastPanel.slice(-3) == "Cfm" && panel.slice(-3) != "Cfm")
                    // lastPanel is Cfm, but open other non-Cfms
                    getNode(this.lastPanel.slice(0, 3)).active = false;
            }
            if (!this.lastPanel || this.lastPanel != panel + "Cfm") {
                // first open, or lastPanel is not panel's cfm
                getNode(panel).active = true;
                this.itemDisplayInit(panel);
            }
        }

        this.lastPanel = panel;
    }

    public itemDisplayDebug() {
        cc.find("Canvas/UI/uiBackpack").opacity = 0;
        this.panelActiveMgr(null, "bpk");
        this.panelActiveMgr(null, "bpk");
        cc.find("Canvas/UI/uiBackpack").opacity = 255;
    }

    public itemDisplayInit(panel: String) {
        // init
        var panelNode = null;
        switch (panel) {
            case "bpk":
                panelNode = cc.find("Canvas/UI/uiBackpack");
                break;
            case "sel":
                panelNode = cc.find("Canvas/UI/uiSell");
                break;
            case "buy":
                panelNode = cc.find("Canvas/UI/uiBuy");
                break;
            case "cft":
                panelNode = cc.find("Canvas/UI/uiCraft");
                break;
            default:
                return;
        }
        var curNode = panelNode.getChildByName(panel + "Cur");

        // Destroy
        this.bpkBgArray = [];
        this.bpkArray = [];
        this.visibleBpkBgArray = [];
        this.visibleBpkArray = [];
        this.lastSelectIdx = -1;

        panelNode.getChildByName(panel + "GridBg").removeAllChildren();
        panelNode.getChildByName(panel + "Grid").removeAllChildren();

        // get Real bg status and items
        var idxCursor = 0;
        for (var i = 0; i < 24; i++) {
            if (panel == "buy") {
                if (i < 4) this.bpkBgArray[i] = 1;
                else this.bpkBgArray[i] = 0;
            } else {
                if (i < gameMgr.levelTable[this.curLevel].maxGrid)
                    this.bpkBgArray[i] = 1;
                else this.bpkBgArray[i] = 0;
            }
        }

        if (panel == "buy") {
            this.bpkArray.push([1, this.curBpk[1]]);
            this.bpkArray.push([2, this.curBpk[2]]);
            this.bpkArray.push([3, this.curBpk[3]]);
            this.bpkArray.push([4, this.curBpk[4]]);
        } else {
            for (var i = 1; i < this.curBpk.length; i++) { // 0 stores money, skip
                if (this.curBpk[i] > 0) {
                    this.bpkArray.push([i, this.curBpk[i]]);
                }
            }
        }

        if (panel == "cft") {
            for (var i = 0; i < 24; i++) {
                if (!this.bpkArray[i] || this.bpkArray[i][0] < 12 || this.bpkArray[i][0] > 15)
                    this.bpkBgArray[i] = 0;
            }
        }

        // Compute pageOffset
        if (gameMgr.levelTable[this.curLevel].maxGrid > 20)
            this.pageOffset = Math.floor((gameMgr.levelTable[this.curLevel].maxGrid - 20) / 4) + 1;
        else this.pageOffset = 0;
        this.itemGridDisplayByPage(panel, panelNode, this.pageOffset);

        // init single display
        panelNode.getChildByName(panel + "BtnConfirm").getComponent(cc.Button).interactable = false;
        curNode.getChildByName("curIcon").getComponent(cc.Sprite).spriteFrame = null;
        curNode.getChildByName("curName").getComponent(cc.Label).string = "";
        curNode.getChildByName("curInfo").getComponent(cc.Label).string = "";
        if (panel == "buy") curNode.getChildByName("curPrice").getComponent(cc.Label).string = "";
        else curNode.getChildByName("curValue").getComponent(cc.Label).string = "";
        curNode.getChildByName("curLoading").getComponent(cc.Label).string = "";
        curNode.getChildByName("curNumber").getComponent(cc.Label).string = "";
    }

    public itemGridDisplayByPage(panel: String, panelNode: cc.Node, pageOffset: number) { //TODO: debug
        var idxCursor = 0;
        this.visibleBpkBgArray = this.bpkBgArray.slice(this.pageOffset * 4, 20 + this.pageOffset * 4);
        this.visibleBpkArray = this.bpkArray.slice(this.pageOffset * 4, 20 + this.pageOffset * 4);

        // Visible bg status and items
        this.visibleBpkBgArray.forEach(e => { // e: gridBgStatus (0: disabled, 1: normal, 2: select)
            var bgPath = (e == 0) ? "./UIPhotos/uiBackpackGridBgDisabled" : "./UIPhotos/uiBackpackGridBgNormal";
            cc.loader.loadRes(bgPath, cc.SpriteFrame, function (err, spriteFrame) {
                var newBg = new cc.Node("visibleGrid" + idxCursor);

                var sprite = newBg.addComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                sprite.sizeMode = cc.Sprite.SizeMode.RAW;
                sprite.trim = false;

                var button = newBg.addComponent(cc.Button);
                var btnAct = new cc.Component.EventHandler();
                btnAct.target = cc.find("Canvas/UI");
                btnAct.component = "gameMgr";
                btnAct.handler = "itemSelect";
                btnAct.customEventData = String(idxCursor++);
                button.clickEvents.push(btnAct);

                newBg.parent = panelNode.getChildByName(panel + "GridBg");
            });
        });

        this.visibleBpkArray.forEach(e => { // e: [itemIdx, number]
            cc.loader.loadRes(gameMgr.itemTable[e[0]].photoURL + "Sm", cc.SpriteFrame, function (err, spriteFrame) {
                var newItem = new cc.Node(gameMgr.itemTable[e[0]].nameEn);

                var sprite = newItem.addComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                sprite.sizeMode = cc.Sprite.SizeMode.RAW;
                sprite.trim = false;

                newItem.parent = panelNode.getChildByName(panel + "Grid");
            });
        });

        panelNode.getChildByName(panel + "Money").getComponent(cc.Label).string = this.curBpk[0];
        panelNode.getChildByName(panel + "Loading").getComponent(cc.Label).string = this.curLoading + "/" + gameMgr.levelTable[this.curLevel].maxLoading;
        panelNode.getChildByName(panel + "LoadingBar").scaleX = this.curLoading / gameMgr.levelTable[this.curLevel].maxLoading;
    }

    public itemSelect(e, selectIdx) { // visibleIdx
        var panel = this.curPanel.slice(0, 3); // avoid cfms
        var panelNode = null;
        switch (panel) {
            case "bpk":
                panelNode = cc.find("Canvas/UI/uiBackpack");
                break;
            case "sel":
                panelNode = cc.find("Canvas/UI/uiSell");
                break;
            case "buy":
                panelNode = cc.find("Canvas/UI/uiBuy");
                break;
            case "cft":
                panelNode = cc.find("Canvas/UI/uiCraft");
                break;
        }
        var curNode = panelNode.getChildByName(panel + "Cur");

        // select normal or select or blank, return
        if (this.visibleBpkBgArray[selectIdx] != 1 || !this.visibleBpkArray[selectIdx]) return;

        // se
        cc.loader.loadRes("./audio/seClickBtn", cc.AudioClip, null, function (err, clip) {
            cc.audioEngine.playEffect(clip, false);
        });

        // right-side display
        var selectItemIdx = this.visibleBpkArray[selectIdx][0];
        cc.loader.loadRes(gameMgr.itemTable[selectItemIdx].photoURL, cc.SpriteFrame, function (err, spriteFrame) {
            var sprite = curNode.getChildByName("curIcon").getComponent(cc.Sprite);
            sprite.spriteFrame = spriteFrame;
            sprite.sizeMode = cc.Sprite.SizeMode.RAW;
            sprite.trim = false;
        });
        panelNode.getChildByName(panel + "BtnConfirm").getComponent(cc.Button).interactable = true;
        curNode.getChildByName("curName").getComponent(cc.Label).string = gameMgr.itemTable[selectItemIdx].nameCh;
        curNode.getChildByName("curInfo").getComponent(cc.Label).string = gameMgr.itemTable[selectItemIdx].info;
        if (panel == "buy") curNode.getChildByName("curPrice").getComponent(cc.Label).string = String(gameMgr.itemTable[selectItemIdx].price);
        else curNode.getChildByName("curValue").getComponent(cc.Label).string = String(gameMgr.itemTable[selectItemIdx].value);
        curNode.getChildByName("curLoading").getComponent(cc.Label).string = String(gameMgr.itemTable[selectItemIdx].loading);
        curNode.getChildByName("curNumber").getComponent(cc.Label).string = String(this.visibleBpkArray[selectIdx][1]);

        // make lastSelect normal
        if (this.lastSelectIdx != -1) {
            var lastSelectNode = panelNode.getChildByName(panel + "GridBg").getChildByName("visibleGrid" + this.lastSelectIdx);
            cc.loader.loadRes("./UIPhotos/uiBackpackGridBgNormal", cc.SpriteFrame, function (err, spriteFrame) {
                var sprite = lastSelectNode.getComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                sprite.sizeMode = cc.Sprite.SizeMode.RAW;
                sprite.trim = false;
            });
            this.visibleBpkBgArray[this.lastSelectIdx] = 1;
            this.bpkBgArray[this.lastSelectIdx + this.pageOffset * 4] = 1;
        }

        // make select select
        var selectNode = panelNode.getChildByName(panel + "GridBg").getChildByName("visibleGrid" + selectIdx);
        cc.loader.loadRes("./UIPhotos/uiBackpackGridBgSelect", cc.SpriteFrame, function (err, spriteFrame) {
            var sprite = selectNode.getComponent(cc.Sprite);
            sprite.spriteFrame = spriteFrame;
            sprite.sizeMode = cc.Sprite.SizeMode.RAW;
            sprite.trim = false;
        });
        this.visibleBpkBgArray[selectIdx] = 2;
        this.bpkBgArray[selectIdx + this.pageOffset * 4] = 2;

        this.curSelectIdx = selectIdx;
        this.itemConfirmInit(panel, selectIdx);
        this.lastSelectIdx = selectIdx;
    }

    public itemConfirmInit(panel: String, selectIdx) {
        panel = panel.slice(0, 3); // get upper panel
        var panelNode = null;
        var verb = ""
        switch (panel) {
            case "bpk":
                panelNode = cc.find("Canvas/UI/uiBackpack");
                verb = "丟棄";
                break;
            case "sel":
                panelNode = cc.find("Canvas/UI/uiSell");
                verb = "販售";
                break;
            case "buy":
                panelNode = cc.find("Canvas/UI/uiBuy");
                verb = "購買";
                break;
            case "cft":
                panelNode = cc.find("Canvas/UI/uiCraft");
                break;
        }
        var cfmNode = panelNode.getChildByName(panel + "Confirm");
        var selectItemIdx = this.visibleBpkArray[selectIdx][0];

        if (panel != "cft") {
            cfmNode.getChildByName("cfmInfo").getComponent(cc.RichText).string
                = "<color=#1f1105>要" + verb + gameMgr.itemTable[selectItemIdx].nameCh + "的數量為</color>";
        } else {
            cfmNode.getChildByName("cfmLeftInfo").getComponent(cc.RichText).string
                = "<color=#1f1105>" + gameMgr.itemTable[selectItemIdx].nameCh + "</color>";
            cc.loader.loadRes(gameMgr.itemTable[selectItemIdx].photoURL + "Sm", cc.SpriteFrame, function (err, spriteFrame) {
                var sprite = cfmNode.getChildByName("cfmLeftIcon").getComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                sprite.sizeMode = cc.Sprite.SizeMode.RAW;
                sprite.trim = false;
            });
            cfmNode.getChildByName("cfmRightInfo").getComponent(cc.RichText).string
                = "<color=#1f1105>" + gameMgr.itemTable[selectItemIdx - 5].nameCh + "</color>";
            cc.loader.loadRes(gameMgr.itemTable[selectItemIdx - 5].photoURL + "Sm", cc.SpriteFrame, function (err, spriteFrame) {
                var sprite = cfmNode.getChildByName("cfmRightIcon").getComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                sprite.sizeMode = cc.Sprite.SizeMode.RAW;
                sprite.trim = false;
            });
        }
    }

    public panelConfirmMgr(e, panel: String) { // click cfmBtn
        panel = panel.slice(0, 3); // get upper panel
        var panelNode = null;
        var verb = ""
        switch (panel) {
            case "bpk":
                panelNode = cc.find("Canvas/UI/uiBackpack");
                break;
            case "sel":
                panelNode = cc.find("Canvas/UI/uiSell");
                break;
            case "buy":
                panelNode = cc.find("Canvas/UI/uiBuy");
                break;
            case "cft":
                panelNode = cc.find("Canvas/UI/uiCraft");
                break;
        }

        var curNode = panelNode.getChildByName(panel + "Cur");
        var cfmNode = panelNode.getChildByName(panel + "Confirm");
        var selectItemIdx = this.visibleBpkArray[this.curSelectIdx][0];
        var itemPrice = gameMgr.itemTable[selectItemIdx].price;
        var itemValue = gameMgr.itemTable[selectItemIdx].value;
        var itemLoading = gameMgr.itemTable[selectItemIdx].loading;
        var itemNumber = Number(curNode.getChildByName("curNumber").getComponent(cc.Label).string);
        var enterNumber = Number(cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string);

        switch (panel) {
            case "bpk":
            case "sel":
                if (enterNumber > itemNumber) {
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = Number(itemNumber);
                } else if (enterNumber < 0) {
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = 0;
                } else {
                    this.curBpk[selectItemIdx] -= enterNumber;
                    this.curLoading -= itemLoading * enterNumber;
                    if (this.curBpk[selectItemIdx] == 0) this.curGrid--;
                    if (panel == "sel") this.curBpk[0] += itemValue * enterNumber;
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = "";
                    this.panelActiveMgr(null, panel);
                    this.panelActiveMgr(null, panel);
                }
                break;
            case "buy":
                if (this.curGrid == gameMgr.levelTable[this.curLevel].maxGrid && this.curBpk[selectItemIdx] == 0) {
                    break; // grid is full
                } else if (enterNumber * itemPrice > this.curBpk[0]
                    || this.curLoading + enterNumber * itemLoading > gameMgr.levelTable[this.curLevel].maxLoading) {
                    var max1 = Math.floor(this.curBpk[0] / itemPrice)
                    var max2 = Math.floor((gameMgr.levelTable[this.curLevel].maxLoading - this.curLoading) / itemLoading);
                    cc.log(max1, max2);
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = Number(Math.min(max1, max2));
                } else if (enterNumber < 0) {
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = 0;
                } else {
                    if (this.curBpk[selectItemIdx] == 0) this.curGrid++;
                    this.curBpk[selectItemIdx] += enterNumber;
                    this.curLoading += itemLoading * enterNumber;
                    this.curBpk[0] -= itemPrice * enterNumber;
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = "";
                    this.panelActiveMgr(null, panel);
                    this.panelActiveMgr(null, panel);
                }
                break;
            case "cft":
                if (itemNumber < 5 || (this.curGrid == gameMgr.levelTable[this.curLevel].maxGrid && this.curBpk[selectItemIdx - 5] == 0)) {
                    break; // grid is full
                } else {
                    this.curBpk[selectItemIdx] -= 5;
                    this.curLoading -= itemLoading * 5;
                    if (this.curBpk[selectItemIdx] == 0) this.curGrid--;
                    if (this.curBpk[selectItemIdx - 5] == 0) this.curGrid++;
                    this.curBpk[selectItemIdx - 5] ++;
                    cfmNode.getChildByName("cfmNumber").getComponent(cc.EditBox).string = "";
                    this.panelActiveMgr(null, panel);
                    this.panelActiveMgr(null, panel);
                }
                break;
        }
    }

    public getSummonTable(){
        return gameMgr.summonTable;
    }

    public getEnemyTable(){
        return gameMgr.enemyTable;
    }

    public getCurAtk(){
        return this.curAtk;
    }

    public getItemNumberById(itemIdx: number) {
        return this.curBpk[itemIdx];
    }

    public setItemNumberById(itemIdx: number, newNumber: number) {
        this.curBpk[itemIdx] = newNumber;
    }

    public itemCreateByDieNode(dieNode: cc.Node) {
        var enemyIdx = 0;
        var createArray = []; // insert itemCoin
        var idxCursor = 0;

        switch (dieNode.name) {
            case "Skeleton":
                enemyIdx = 0;
                break;
            case "Skeleton_Shield":
                enemyIdx = 1;
                break;
            case "Skeleton_Spear":
                //temp
                enemyIdx = 5;
                break;
            case "Skeleton_Archer":
                enemyIdx = 3;
                break;
            case "Skeleton_Mage":
                enemyIdx = 4;
                break;
            case "Skeleton_Boss":
                enemyIdx = 5;
                break;
        }

        var ranNum = Math.random();
        gameMgr.enemyTable[enemyIdx].genItem.forEach(e => {
            if (e[1] > ranNum) { // e[1]: prob.
                createArray.push(e[0]); // e[0]: itemIdx
            }
        });

        createArray.push(0); // itemCoin

        createArray.forEach(e => { // e: itemIdx
            cc.loader.loadRes(gameMgr.itemTable[e].photoURL + "Sm", cc.SpriteFrame, function (err, spriteFrame) {
                var newItem = new cc.Node(gameMgr.itemTable[e].nameEn);
                newItem.x = dieNode.x + createArray.length * (-32) + idxCursor * 64 + 32;
                newItem.y = dieNode.y + 32;

                var sprite = newItem.addComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                sprite.sizeMode = cc.Sprite.SizeMode.RAW;
                sprite.trim = false;

                var rigidBody = newItem.addComponent(cc.RigidBody);
                rigidBody.enabledContactListener = true;
                rigidBody.type = cc.RigidBodyType.Kinematic;

                var physicsBoxCollider = newItem.addComponent(cc.PhysicsBoxCollider);
                physicsBoxCollider.tag = 300 + e;
                physicsBoxCollider.sensor = true;

                newItem.parent = dieNode.parent;
                idxCursor++;
            });
        });
    }

    public itemPickUpByNode(node: cc.Node) {
        var levelInfo = gameMgr.levelTable[this.curLevel];
        var itemIdx = node.getComponent(cc.PhysicsBoxCollider).tag % 300;

        if (itemIdx == 0) { // itemCoin: 25~75
            this.curBpk[itemIdx] += Math.floor(25 + Math.random() * 50);
            cc.loader.loadRes("./audio/sePickUp", cc.AudioClip, null, function (err, clip) {
                cc.audioEngine.playEffect(clip, false);
            });
        } else {
            cc.log(this.curGrid)
            if (this.curGrid >= levelInfo.maxGrid
                || this.curLoading + gameMgr.itemTable[itemIdx].loading >= levelInfo.maxLoading) return;
            cc.log("Pick up " + gameMgr.itemTable[itemIdx].nameCh);
            if (this.curBpk[itemIdx] == 0) this.curGrid++;
            this.curBpk[itemIdx]++;
            this.curLoading += gameMgr.itemTable[itemIdx].loading;
            cc.loader.loadRes("./audio/sePickUp", cc.AudioClip, null, function (err, clip) {
                cc.audioEngine.playEffect(clip, false);
            });
        }
        node.destroy();
    }

    public itemUse(itemIdx: number) {
        if (itemIdx < 1 || itemIdx > 6 || this.curBpk[itemIdx] <= 0) return;
        cc.loader.loadRes("./audio/seItemUse", cc.AudioClip, null, function (err, clip) {
            cc.audioEngine.playEffect(clip, false);
        });

        this.curBpk[itemIdx]--;
        switch (itemIdx) {
            case 1:
                this.addHpBy(25);
                break;
            case 2:
                this.addHpBy(100);
                break;
            case 3:
                this.addHpBy(999);
                break;
            case 4:
                this.getSts(1);
                break;
            case 5:
                this.cancelSts(0);
                break;
            case 6:
                this.getSts(2);
                break;
        }
    }

    public saveInfo(){
        var newInfo = {
            playerName: this.playerName,
            lastLevel: this.curLevel,
            lastHp: this.curHp,
            lastExp: this.curExp,
            lastSts: this.curSts,
            lastBpk: this.curBpk
        }
        cc.find("persistInfo").getComponent("persistInfo").setInfo(newInfo);
    }
}