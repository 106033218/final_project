const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    private info = {
        playerName: "The one",
        lastLevel: 1,
        lastHp: 250,
        lastExp: 0,
        lastSts: [0, 0, 0], //remaining seconds of poison, attack, avoid
        lastBpk: [
            1450,
            2, 2, 1, 1, 1, 0,
            3, 0, 0, 0,
            0, 4, 10, 2, 0,
            0, 0, 0, 0, 0, 0, 0,
            0, 0
        ]
    }

    onLoad () {
        cc.game.addPersistRootNode(this.node);
    }

    public getInfo(){
        return this.info;
    }

    public setInfo(newInfo){
        this.info = newInfo;
    }
}