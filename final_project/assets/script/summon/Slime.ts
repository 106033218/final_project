// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { gameMgr } from "../gameMgr";

const {ccclass, property} = cc._decorator;
enum status_list    //用三種狀態去決定目前要播哪個動畫
{
  run = 1,
  at = 2,
  dead = 3
}

@ccclass
export default class NewClass extends cc.Component {
    @property()
    moveSpeed: number = 300;                 //角色水平移動速度 ************ 各角色不同

    @property(gameMgr)/**/
    gameMgr: gameMgr;

    private anim: cc.Animation = null;

    private moveDir = 0;                    //移動方向
    private velocity: cc.RigidBody = null;  //速度控制
    private status: number = 1;             //角色狀態
    private at_box = null;                  //角色攻擊的碰撞格 ************
    private target = null;                  //敵人節點
    private dead: boolean = false;          //角色是否死亡
    private findTarget: boolean = false;    //是否找到目標
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.at_box = this.node.getChildByName("at");
        cc.log(this.at_box);
    }

    start () {
        this.anim = this.getComponent(cc.Animation);
        this.velocity = this.getComponent(cc.RigidBody);
    }

    update (dt) {
        
        if(!this.findTarget){
            this.moveDir = 1;
        } 
        else{
            if(this.target.active==false) this.findTarget = false
            this.moveDir = (this.node.position.x < this.target.position.x) ?1 :-1 ;
            if(Math.abs(this.node.position.x-this.target.position.x)<50){ 
                this.moveDir = 0;
                this.status = status_list.at;                                       //切換為攻擊狀態
            }
            
        }
        if(this.moveDir!=0) this.node.scaleX = (this.moveDir > 0) ? 1 : -1;     //決定角色方向
        this.velocity.linearVelocity = cc.v2(this.moveSpeed*this.moveDir, this.velocity.linearVelocity.y);  //角色在run時移動狀態
           
        this.Animation();       //播放動畫
    }
    
    Animation(){
        switch(this.status){
            case status_list.run:
                if(!this.anim.getAnimationState("run").isPlaying)
                    this.anim.play("run");
                break;
            case status_list.at:
                if(!this.anim.getAnimationState("run").isPlaying)
                this.anim.play("run");
                break;
            case status_list.dead:
                if(!this.dead){
                    this.anim.play("die");
                    this.dead = true;
                }
                   
                break;
            default:
                break;
        }
    }


    attack(){                                   //這個要用關鍵幀來調用函數，從動畫窗格新增在角色把武器砍出去那幀
        this.at_box.active = true;
        this.anim.once('finished',function(){   //攻擊動畫結束時回到run狀態，取消攻擊碰種格
            if(!this.dead) this.status = status_list.run;
            this.at_box.active = false;
        }, this);
    }


    onCollisionEnter(other, self) {
        if(!this.findTarget){
            this.target = other.node.parent;
            this.findTarget = true;
        }
    }

}
