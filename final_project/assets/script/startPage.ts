// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    private loginBtn:cc.Node = null;
    private signupBtn:cc.Node = null;
    // LIFE-CYCLE CALLBACKS:


    onLoad () {
        this.initChildNode();
    }
    start () {
        this.loginBtn.on(cc.Node.EventType.MOUSE_UP, ()=>{
            cc.director.loadScene("signInPage");
            cc.log("load signInPage");
        }, this);
        this.signupBtn.on(cc.Node.EventType.MOUSE_UP, ()=>{
            cc.director.loadScene("signUpPage");
            cc.log("load signUpPage")
        }, this);  
        cc.log(this.loginBtn);
        cc.log(this.signupBtn);
    }

    // update (dt) {}

    initChildNode(){
        this.loginBtn = this.node.getChildByName("loginBtn");
        this.signupBtn = this.node.getChildByName("signupBtn");
    }
}
